//! Test suite for the Web and headless browsers.

#![cfg(target_arch = "wasm32")]

extern crate wasm_bindgen_test;
use wasm_bindgen_test::*;

wasm_bindgen_test_configure!(run_in_browser);

#[wasm_bindgen_test]
fn pass() {
    assert_eq!(1 + 1, 2);
}

extern crate rust_cid_npm;
use rust_cid_npm::*;


#[wasm_bindgen_test]
fn test_cid_browser(){
    // Known test case for simple string "abcdefg\n" as a Vec<u8> -- tool that can help: https://onlineunicodetools.com/convert-unicode-to-utf8  -- note that u8 is ecoded in decimal!!!
    let content = vec![97, 98, 99, 100, 101, 102, 103, 10];
    let cidv0 = "QmYhMRHZAceoM8L21yqBcJjk1TXasZzHw4g1qzTSHBLq1c";
    let cidv1 = "bafybeiez4kcddcze2tyxsrxzp3lbtx6jyue3mhbg4uo2t2mmdfywwp3sxm";

    let expected = String::from(format!("{{\"CIDv0\":\"{}\",\"CIDv1\":\"{}\"}}",cidv0,cidv1));

    assert_eq!(expected, get_cid(content));    
}