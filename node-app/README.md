# Example node app using `rust_cid_npm`

**Simple example getting CIDs of file loaded into the browser using Rust->WASM for _fast_ generation of the CIDs**

See `index.js` for an example of how to access CIDs as a JSON object.

## Usage

**First use, run: `npm install`**

- Run `npm start` from this location as you working directory to use the example app as is
- Run `npm run build` to generate `webpack` files into the `./dist` directory.